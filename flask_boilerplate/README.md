## Set Up app

1. `pip install -r requirements.txt`
2. `export FLASK_APP=flask_example` note this command may vary based on OS https://flask.palletsprojects.com/en/2.0.x/tutorial/factory/
3. `flask shell`
4. inside flask shell run the follwing to setup the database
  * `from flask_example.models import Person`
  * `from flask_example.models import Address`
  * `from flask_example.database import db`
  * `db.create_all()`
